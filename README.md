# Overview

This repository contains tests for a real world ecommerce app : amazon.com

# Test plan

The test plan can be found at the following link

[Test plan](https://docs.google.com/document/d/1Tt5QyJ36Q1Nu7lzd1VW9Q3UExiGEJI2lzVLc8zeEAO8/edit?usp=sharing)

# Prerequisites

The only requirement for this project is to have node.js

# Installation

```bash
npm install
```

# Start cypress

```bash
npx cypress open
```

## Tests

| Type | Location                                   |
| ---- | ------------------------------------------ |
| api  | [cypress/tests/api](./cypress/tests/api)   |
| ui   | [cypress/tests/ui](./cypress/tests/ui)     |
| unit | [cypress/tests/unit](./cypress/tests/unit) |

## Reporting

- link ...
